<?php

namespace Agentor\Hangouts;

use GuzzleHttp\Client as HttpClient;

class Client
{
    protected $convid;
    protected $url;
    protected $key;

    public function __construct($url, $key, $convid = null)
    {
        $this->url = $url;
        $this->key = $key;
        $this->convid = $convid;
    }

    public function setConvid($convid)
    {
        $this->convid = $convid;
    }

    public function setUrl($url)
    {
        $this->url = $url;
    }

    public function setKey($key)
    {
        $this->key = $key;
    }

    public function test()
    {
        $this->send('test the hangouts api ' . date('Y-m-d H:i:s'));
    }

    public function send($content, $convid = null)
    {
        $this->call($content, $convid);
    }

    protected function call($content, $convid = null)
    {
        $params = [
            "key" => $this->key,
            "sendto" => ($convid ? $convid : $this->convid),
            'content' => $content,
        ];

        $client = new HttpClient();
        $response = $client->request(
            'POST',
            $this->url,
            [
                'json' => $params,
                'headers' => [
                    'Accept' => 'application/json',
                ],
                'verify' => false,
            ]
        );

        return $response;
    }
}